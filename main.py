import math
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


HISTORY_LENGTH = 25

# G = pow(10, -11)
# AXIS_RANGE = 500000

G = 0.001
AXIS_RANGE = 5


class Body:
    def __init__(self, pos, v, m):
        self.pos = pos
        self.v = v
        self.m = m
        self.direction_vector = [0, 0, 0]
        # first item is the most recent position, last item is the oldest
        self.previous_positions = []

    def apply_force(self, F, direction_vector):
        acceleration = F/self.m
        self.direction_vector = direction_vector

        self.update_v(acceleration)

    def update_v(self, delta_v):
        self.v[0] += delta_v * self.direction_vector[0]
        self.v[1] += delta_v * self.direction_vector[1]
        self.v[2] += delta_v * self.direction_vector[2]

    def tick_move_2d(self):
        self.pos[0] += self.v[0]
        self.pos[1] += self.v[1]
        self.pos[2] += self.v[2]

    def get_direction_vector_2d(self, body):
        r = calculate_distance_2d(self, body)
        x = (self.pos[0] - body.pos[0]) / r
        y = (self.pos[1] - body.pos[1]) / r
        z = (self.pos[2] - body.pos[2]) / r

        return [x, y, z]

    def save_previous_position(self):
        if len(self.previous_positions) >= HISTORY_LENGTH:
            self.previous_positions.pop()

        pos = self.pos.copy()
        self.previous_positions.insert(0, pos)


def calculate_distance_2d(b1, b2):
    return math.sqrt((b1.pos[0] - b2.pos[0])**2 + (b1.pos[1] - b2.pos[1])**2 + (b1.pos[2] - b2.pos[2])**2)


def calculate_gravitational_force(b1, b2):
    r = calculate_distance_2d(b1, b2)
    # print("distance", r)
    return -G * b1.m * b2.m / r**2


def apply_force(b1, b2):
    F = calculate_gravitational_force(b1, b2)
    # print("force", F)
    b1.apply_force(F, b1.get_direction_vector_2d(b2))
    b2.apply_force(F, b2.get_direction_vector_2d(b1))


# STABLE STATE
# body_1 = Body([-4, 0], [0, 0.2], 200)
# body_2 = Body([4, 0], [0, -0.2], 200)

# Earth and Moon system
# system = [
#     Body([0, 0, 0], [0, 0, 0], 6*pow(10, 24)),
#     Body([3*pow(10, 5), 0, 0], [-pow(10,2), pow(10, 4), 0], 6*pow(10, 24)/81),
# ]

# interesing system
# system = [
#     Body([-3, 0, -3], [0, 0.2, 0], 200),
#     Body([3, 0, 3], [0, -0.2, 0], 250),
#     Body([0, 1, 3], [0, 0.1, 0.1], 150)
# ]

system = [
    # Body([2, 0.5, 1], [0.1, -0.1, 0], 200),
    Body([-3, 0, -3], [0, 0.2, 0], 200),
    Body([3, 0, 3], [0, -0.2, 0], 250),
    Body([0, 1, 3], [0, 0.1, 0.1], 150)
]


plt.ion()
fig = plt.figure()
ax = Axes3D(fig)


while True:
    # ax = plt.gca()

    ax.set_axis_off()
    ax.set_facecolor('black')
    ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
    ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
    ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))

    plt.ylim([-AXIS_RANGE, AXIS_RANGE])
    plt.xlim([-AXIS_RANGE, AXIS_RANGE])
    ax.set_zlim(-AXIS_RANGE, AXIS_RANGE)

    x = [b.pos[0] for b in system]
    y = [b.pos[1] for b in system]
    z = [b.pos[2] for b in system]
    sizes = [b.m for b in system]

    # history = [b.previous_positions for b in system]

    ax.scatter(x, y, z, sizes, c='orange', depthshade=False)

    for b in system:
        # hist_x = [p[0] for p in b.previous_positions]
        # hist_y = [p[1] for p in b.previous_positions]
        # hist_z = [p[2] for p in b.previous_positions]
        # colors = ['#ff6c40' + str(i*10) for i in range(len(b.previous_positions))]
        # ax.plot(hist_x, hist_y, hist_z, c='orange', alpha=0.5)

        lower = 0
        upper = 100
        length = HISTORY_LENGTH
        alphas = [lower + x * (upper - lower) / length for x in range(length)]
        colors_alpha = ['#ff6c40' + str(int(alpha)).rjust(2, '0') for alpha in alphas]
        colors_alpha.reverse()

        for i in range(len(b.previous_positions)-1):
            hist_x = [b.previous_positions[i][0], b.previous_positions[i+1][0]]
            hist_y = [b.previous_positions[i][1], b.previous_positions[i+1][1]]
            hist_z = [b.previous_positions[i][2], b.previous_positions[i+1][2]]
            ax.plot(hist_x, hist_y, hist_z, c=colors_alpha[i], marker=None)


    plt.draw()
    plt.pause(0.005)
    ax.clear()

    for i, body_1 in enumerate(system):
        for j in range(i, len(system)):
            if i is not j:
                body_2 = system[j]
                apply_force(body_1, body_2)

    for body in system:
        body.save_previous_position()
        body.tick_move_2d()



